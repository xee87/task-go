@php
    $logo = \App\Models\Utility::get_file('logo/');
    $path_imgs = \App\Models\Utility::get_file('/');
      $details        = Auth::user()->decodeDetails();
@endphp


@php
    $logo_path=\App\Models\Utility::get_file('/');
@endphp

<!-- Sidenav header -->
<div class="sidenav-header d-flex align-items-center">
    <a class="navbar-brand" href="#">
        @if(Auth::user()->type == 'admin')
            <img src="{{$logo.'logo.png'}}" class="navbar-brand-img" alt="...">
        @else

            @if (Auth::user()->mode == 'light')
                <img src="{{$path_imgs.$details['dark_logo']}}" class="navbar-brand-img" alt="...">
            @else
                <img src="{{$path_imgs.$details['light_logo']}}" class="navbar-brand-img" alt="...">
            @endif



        @endif
    </a>
    <div class="ml-auto">
        <!-- Sidenav toggler -->
        <div class="sidenav-toggler sidenav-toggler-dark d-md-none" data-action="sidenav-unpin"
             data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
                <i class="sidenav-toggler-line bg-white"></i>
                <i class="sidenav-toggler-line bg-white"></i>
                <i class="sidenav-toggler-line bg-white"></i>
            </div>
        </div>
    </div>
</div>
<!-- User mini profile -->
<div class="sidenav-user d-flex flex-column align-items-center justify-content-between text-center">
    <!-- Avatar -->
    <div>
        <a href="#" class="avatar rounded-circle avatar-xl">


            @if(Auth::user()->avatar)
                <img src="{{$path_imgs. Auth::user()->avatar}}" class="avatar rounded-circle avatar-xl">
            @else
                <img class="avatar rounded-circle avatar-xl" {{ Auth::user()->img_avatar }} />
            @endif


        </a>
        <div class="mt-4">
            <h5 class="mb-0 text-white">{{ Auth::user()->name }}</h5>
            <span class="d-block text-sm text-white opacity-8 mb-3">{{ Auth::user()->email }}</span>
            @if(Auth::user()->type != 'admin')
                <a href="{{ route('users.info',Auth::user()->id) }}"
                   class="btn btn-sm btn-white btn-icon rounded-pill shadow hover-translate-y-n3">
                    <span class="btn-inner--icon"><i class="fas fa-coins"></i></span>
                    <span class="btn-inner--text">{{__('My Overview')}}</span>
                </a>
            @endif
        </div>
    </div>
    <!-- User info -->
    <!-- Actions -->

        <div class="w-100 mt-4 actions">
            @if(Auth::user()->type != 'admin')
{{--            <a href="{{ route('profile') }}" data-toggle="tooltip" data-placement="bottom"--}}
{{--               data-original-title="{{__('Profile')}}" class="action-item action-item-lg text-white pl-0 mx-3">--}}
{{--                <i class="fas fa-user"></i>--}}
{{--            </a>--}}
                <a href="{{ route('contractclient.index') }}" data-toggle="tooltip" data-placement="bottom"
                   data-original-title="{{__('Contracts')}}" class="action-item action-item-lg text-white mx-3">
                    <i class="fas fa-file-contract"></i>
                </a>
                <a href="{{ route('invoices.index') }}" data-toggle="tooltip" data-placement="bottom"
                   data-original-title="{{__('Invoices')}}" class="action-item action-item-lg text-white mx-3">
                    <i class="fas fa-receipt"></i>
                </a>
            <a href="{{ route('expense.list') }}" data-toggle="tooltip" data-placement="bottom"
               data-original-title="{{__('Expense')}}" class="action-item action-item-lg text-white mx-3">
                <i class="fas fa-receipt"></i>
            </a>

            @endif
                <a href="javascript:void(0)" id="grid_view" data-toggle="tooltip" data-placement="bottom"
                   data-original-title="{{__('Grid')}}" class="action-item action-item-lg text-white mx-3">
                    <i class="fa fa-th" aria-hidden="true"></i>
                </a>
                <a href="javascript:void(0)" id="list_view" data-toggle="tooltip" data-placement="bottom"
                   data-original-title="{{__('List')}}" class="action-item action-item-lg text-white mx-3">
                    <i class="fa fa-list" aria-hidden="true"></i>
                </a>
        </div>


</div>
<!-- Application nav -->
<div class="nav-application clearfix row overflow-sidenav" id="gridView">

    <br>
    <a href="{{ route('home') }}"
       class="btn btn-square text-sm {{ (Request::route()->getName() == 'home') ? 'active' : '' }}">
        <span class="btn-inner--icon d-block"><i class="fas fa-home fa-2x"></i></span>
        <span class="btn-inner--icon d-block pt-2">{{__('Home')}}</span>
    </a>
    @if(Auth::user()->type != 'admin')
        <a href="{{ route('projects.index') }}"
           class="btn btn-square text-sm {{ request()->is('project*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-project-diagram fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Projects')}}</span>
        </a>
        <a href="{{ route('taskBoard.view') }}"
           class="btn btn-square text-sm {{ request()->is('taskboard*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-tasks fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Tasks')}}</span>
        </a>
        <a href="{{ route('users') }}" class="btn btn-square text-sm {{ request()->is('users*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-users fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Members')}}</span>
        </a>
        @if ( Auth::user()->type == 'owner' || \App\Models\User::GetusrRole()['role'] == 'client')
            <a href="{{ route('contractclient.index') }}"
               class="btn btn-square text-sm {{ request()->is('contractclient*') ? 'active' : '' }}">
                <span class="btn-inner--icon d-block"><i class="fas fa-file-contract fa-2x"></i></span>
                <span class="btn-inner--icon d-block pt-2">{{__('Contract')}}</span>
            </a>
        @endif
        <a href="{{ route('invoices.index') }}"
           class="btn btn-square text-sm {{ request()->is('invoices*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-receipt fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Invoices')}}</span>
        </a>
        <a href="{{ route('task.calendar',['all']) }}"
           class="btn btn-square text-sm {{ request()->is('calendar*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-calendar-week fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Calendar')}}</span>
        </a>
        <a href="{{ route('timesheet.list') }}"
           class="btn btn-square text-sm {{ request()->is('timesheet-list') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-clock fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Timesheet')}}</span>
        </a>
        <a href="{{ route('time.tracker') }}"
           class="btn btn-square text-sm {{ request()->is('time-tracker') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-stopwatch fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Tracker')}}</span>
        </a>
        <a href="{{ url('chats') }}" class="btn btn-square text-sm {{ request()->is('chats') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-comments fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Messenger')}}</span>
        </a>

        @if(Auth::user()->type != 'admin')
            <a href="{{ route('report_project.index') }}" class="btn btn-square text-sm  {{ request()->is('report_project*') ? 'active' : '' }}">
                <span class="btn-inner--icon d-block"><i class="fas fa-chart-line fa-2x"></i></span>
                <span class="btn-inner--icon d-block pt-2">{{__('Project Report')}}</span>

            </a>
        @endif
        <a href="{{ route('zoommeeting.index') }}"
           class="btn btn-square text-sm {{ request()->is('zoommeeting*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-video fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Zoom Meeting')}}</span>
        </a>
    @endif
    @if(\Auth::user()->type == 'admin')
        <a href="{{route('lang',basename(App::getLocale()))}}"
           class="btn btn-square text-sm {{ request()->is('lang*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-language fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Language')}}</span>
        </a>
        <a href="{{route('email_template.index')}}"
           class="btn btn-square text-sm {{ request()->is('email_template*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-envelope fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Email Template')}}</span>
        </a>
        {{--        <a href="{{ route('plans.index') }}" class="btn btn-square text-sm {{ request()->is('plans*') ? 'active' : '' }}">--}}
        {{--            <span class="btn-inner--icon d-block"><i class="fas fa-award fa-2x"></i></span>--}}
        {{--            <span class="btn-inner--icon d-block pt-2">{{__('Plans')}}</span>--}}
        {{--        </a>--}}
        <a href="{{ route('plan_request.index') }}"
           class="btn btn-square text-sm {{ request()->is('plan_request*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-paper-plane fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Plan Request')}}</span>
        </a>
        <a href="{{ route('order_list') }}"
           class="btn btn-square text-sm {{ request()->is('orders') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-file-invoice fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Orders')}}</span>
        </a>
        <a href="{{ route('coupons.index') }}"
           class="btn btn-square text-sm {{ request()->is('coupons*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-ticket-alt fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Coupons')}}</span>
        </a>

    @endif
    @if(\Auth::user()->type == 'admin'  || \Auth::user()->type == 'owner')
        <a href="{{ route('settings') }}"
           class="btn btn-square text-sm {{ request()->is('settings*') ? 'active' : '' }}">
            <span class="btn-inner--icon d-block"><i class="fas fa-cogs fa-2x"></i></span>
            <span class="btn-inner--icon d-block pt-2">{{__('Settings')}}</span>
        </a>
    @endif
</div>

<div class="nav-application clearfix row overflow-sidenav listView d-none" id="listView">

    <br>
    <a href="{{ route('home') }}"
       class="btn btn-square text-sm {{ (Request::route()->getName() == 'home') ? 'active' : '' }}">
        <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-home"></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Home')}}</span>
        </div>

    </a>
    @if(Auth::user()->type != 'admin')
        <a href="{{ route('projects.index') }}"
           class="btn btn-square text-sm {{ request()->is('project*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-project-diagram "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Projects')}}</span>
            </div>
        </a>
        <a href="{{ route('taskBoard.view') }}"
           class="btn btn-square text-sm {{ request()->is('taskboard*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-tasks "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Tasks')}}</span>
            </div>
        </a>
        <a href="{{ route('users') }}" class="btn btn-square text-sm {{ request()->is('users*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-users "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Members')}}</span>
            </div>
        </a>
        @if ( Auth::user()->type == 'owner' || \App\Models\User::GetusrRole()['role'] == 'client')
            <a href="{{ route('contractclient.index') }}"
               class="btn btn-square text-sm {{ request()->is('contractclient*') ? 'active' : '' }}">
                <div class="row">
                <span class="btn-inner--icon d-block ml-2"><i class="fas fa-file-contract "></i></span>
                <span class="btn-inner--icon d-block ml-3">{{__('Contract')}}</span>
                </div>
            </a>
        @endif
        <a href="{{ route('invoices.index') }}"
           class="btn btn-square text-sm {{ request()->is('invoices*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-receipt "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Invoices')}}</span>
            </div>
        </a>
        <a href="{{ route('task.calendar',['all']) }}"
           class="btn btn-square text-sm {{ request()->is('calendar*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-calendar-week "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Calendar')}}</span>
            </div>
        </a>
        <a href="{{ route('timesheet.list') }}"
           class="btn btn-square text-sm {{ request()->is('timesheet-list') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-clock "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Timesheet')}}</span>
            </div>
        </a>
        <a href="{{ route('time.tracker') }}"
           class="btn btn-square text-sm {{ request()->is('time-tracker') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-stopwatch "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Tracker')}}</span>
            </div>
        </a>
        <a href="{{ url('chats') }}" class="btn btn-square text-sm {{ request()->is('chats') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-comments "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Messenger')}}</span>
            </div>
        </a>

        @if(Auth::user()->type != 'admin')
            <a href="{{ route('report_project.index') }}" class="btn btn-square text-sm  {{ request()->is('report_project*') ? 'active' : '' }}">
                <div class="row">
                <span class="btn-inner--icon d-block ml-2"><i class="fas fa-chart-line "></i></span>
                <span class="btn-inner--icon d-block ml-3">{{__('Project Report')}}</span>
                </div>

            </a>
        @endif
        <a href="{{ route('zoommeeting.index') }}"
           class="btn btn-square text-sm {{ request()->is('zoommeeting*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-video "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Zoom Meeting')}}</span>
            </div>
        </a>
    @endif
    @if(\Auth::user()->type == 'admin')
        <a href="{{route('lang',basename(App::getLocale()))}}"
           class="btn btn-square text-sm {{ request()->is('lang*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-language "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Language')}}</span>
            </div>
        </a>
        <a href="{{route('email_template.index')}}"
           class="btn btn-square text-sm {{ request()->is('email_template*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-envelope "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Email Template')}}</span>
            </div>
        </a>
        {{--        <a href="{{ route('plans.index') }}" class="btn btn-square text-sm {{ request()->is('plans*') ? 'active' : '' }}">--}}
        {{--            <span class="btn-inner--icon d-block"><i class="fas fa-award "></i></span>--}}
        {{--            <span class="btn-inner--icon d-block ml-3">{{__('Plans')}}</span>--}}
        {{--        </a>--}}
        <a href="{{ route('plan_request.index') }}"
           class="btn btn-square text-sm {{ request()->is('plan_request*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-paper-plane "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Plan Request')}}</span>
            </div>
        </a>
        <a href="{{ route('order_list') }}"
           class="btn btn-square text-sm {{ request()->is('orders') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-file-invoice "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Orders')}}</span>
            </div>
        </a>
        <a href="{{ route('coupons.index') }}"
           class="btn btn-square text-sm {{ request()->is('coupons*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-ticket-alt "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Coupons')}}</span>
            </div>
        </a>

    @endif
    @if(\Auth::user()->type == 'admin'  || \Auth::user()->type == 'owner')
        <a href="{{ route('settings') }}"
           class="btn btn-square text-sm {{ request()->is('settings*') ? 'active' : '' }}">
            <div class="row">
            <span class="btn-inner--icon d-block ml-2"><i class="fas fa-cogs "></i></span>
            <span class="btn-inner--icon d-block ml-3">{{__('Settings')}}</span>
            </div>
        </a>
    @endif
</div>


@push('script')
    <script>
        $(document).ready(function() {
            $('#list_view').click(function (event) {
                event.preventDefault();

                $('#grid_view').removeClass('d-none');
                $('#list_view').addClass('d-none');
                $('#gridView').addClass('d-none');
                $('#listView').removeClass('d-none');
                localStorage.setItem("sidenav_view", "List View");
                if($('body').find('.tooltip').hasClass('show')){
                    $('body').find('.tooltip').remove();
                }
            });

            $('#grid_view').click(function (event) {
                event.preventDefault();
                $('#list_view').removeClass('d-none');
                $('#grid_view').addClass('d-none');
                $('#listView').addClass('d-none');
                $('#gridView').removeClass('d-none');
                localStorage.setItem("sidenav_view", "Grid View")
                if($('body').find('.tooltip').hasClass('show')){
                    $('body').find('.tooltip').remove();
                }
            });

            let geValue = localStorage.getItem("sidenav_view") || "";
            if (geValue === 'List View') {
                $('#list_view').trigger("click");
            } else {
                $('#grid_view').trigger("click");
            }
        });
    </script>
@endpush
