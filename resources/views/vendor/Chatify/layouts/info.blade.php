
@php
$logo   = \App\Models\Utility::get_file('/');
$avatar = \App\Models\Utility::get_file('/avatars/');
@endphp

@if(!empty(\Auth::user()->avatar))
    <div class="avatar av-l" style="background-image: url('{{$logo.Auth::user()->avatar}}');">
    </div>
@else
    <div class="avatar av-l"
         style="background-image:  url('{{$avatar.'avatar.png'}}');">
    </div>
@endif
<p class="info-name">{{ config('chatify.name') }}</p>
<div class="messenger-infoView-btns">
    <a href="#" class="danger delete-conversation"><i class="fas fa-trash-alt"></i> Delete Conversation</a>
</div>

<div class="messenger-infoView-shared">
    <p class="messenger-title">shared photos</p>
    <div class="shared-photos-list"></div>
</div>
